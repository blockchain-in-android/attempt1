package net.anandu.attempt1;

import android.view.View;
import android.widget.TextView;

public class ViewHolder {
    public View tvBox;
    public TextView tvDisplayText;
    public TextView tvMeta;
}
