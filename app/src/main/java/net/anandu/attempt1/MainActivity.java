package net.anandu.attempt1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import net.anandu.blocklib.network.NetworkInterface;
import net.anandu.blocklib.network.testnet.TestNet;
import net.anandu.blocklib.BlockLib;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ItemListAdapter adapter;
    final List<Item> items = new ArrayList<Item>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list);

        adapter = new ItemListAdapter(this, items);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(MainActivity.this, items.get(i).toString(), Toast.LENGTH_SHORT).show();
            }
        });
        addLogItem(Item.ItemType.STARTUP, "Startup called", "startup");


        NetworkInterface n = new TestNet("anandu", "123") {
            @Override
            public void onMessageReceived(String message) {
                Log.d("ANANDU", "onMessageReceived: message");
//                this won't work because view issue
//                addLogItem(Item.ItemType.CONNECTED, message, message);
            }
        };

        Button clickButton = (Button) findViewById(R.id.offer_button);
        clickButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Connecting", Toast.LENGTH_SHORT).show();
                n.connect();
            }
        });
        Button sendMessage = (Button) findViewById(R.id.message);
        sendMessage.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Sending message", Toast.LENGTH_SHORT).show();
                n.broadcast("HI there");
            }
        });

        /*
         * START THE LIBRARY
         */
        Context c = getApplicationContext();
        BlockLib b = new BlockLib();
        b.init(c);
    }

    private void addLogItem(Item.ItemType type, String title, String body) {
        items.add(new Item(type, title, body));
        adapter.notifyDataSetChanged();
    }

}