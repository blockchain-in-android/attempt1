package net.anandu.attempt1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ItemListAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private final List<Item> items;

    public ItemListAdapter(Context context, List<Item> items) {
        this.inflater = LayoutInflater.from(context);
        this.items = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Item getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.items.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Item item = getItem(position);

        if(convertView == null) {
            convertView = this.inflater.inflate(R.layout.log_listitem, parent, false);
            // why? performance reasons ig
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.tvBox = (View) convertView.findViewById(R.id.box);
            viewHolder.tvDisplayText = (TextView) convertView.findViewById(R.id.title);
            viewHolder.tvMeta = (TextView) convertView.findViewById(R.id.body);

            convertView.setTag(viewHolder);
        }

        final ViewHolder viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.tvDisplayText.setText(String.format("%s: %s",item.getItemType().toString(), item.getTitle()));
        viewHolder.tvMeta.setText(item.getBody());
        viewHolder.tvBox.setBackgroundColor(item.getColor());

        return convertView;
    }

}
