package net.anandu.attempt1;

import android.graphics.Color;

public class Item {

    public enum ItemType {
        STARTUP,
        CONNECTED,
        DISCONNECTED,
        ERROR,
    }

    private ItemType itemType;
    private String title;
    private String body;

    public Item(ItemType itemType, String title, String body) {
        this.itemType = itemType;
        this.title = title;
        this.body = body;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    public int getColor() {
        if ( ItemType.STARTUP.equals(itemType)) {
            return Color.parseColor("#e51400");
        }
        return Color.parseColor("blue");
    }

    @Override
    public String toString() {
        return "{" +
                "itemType=" + itemType +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
